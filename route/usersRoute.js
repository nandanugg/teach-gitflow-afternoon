const express = require('express')
const UserControler = require('../controller/usersController')
const user = new UserControler()
const app = express.Router()

// Tolong uncomment ini 😉
app.put('/:id', async (req, res, next) => {
  const { id } = req.params
  await user.edit(id, req.body).catch(next)
  res.send("Yes") 
})

module.exports = app